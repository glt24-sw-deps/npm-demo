# Random Notes

I use node in a container:

- `sudo docker run --rm -it -v "$(pwd):/work" --entrypoint "" --workdir /work -u $(id -u):$(id -g) -p 127.0.0.1:3000:3000 node:21 /bin/bash`
- `npx create-react-app my-app`
