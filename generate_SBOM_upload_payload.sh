#!/bin/bash

cat > payload.json <<__HERE__
{
    "project": "${SBOM_UPLOAD_PROJECT_UUID}",
    "bom": "$(base64 -w 0 sbom.json)"
}
__HERE__
