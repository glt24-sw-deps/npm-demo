# NPM Demo

A little demo project do demonstrate dependency management for node projects.

The resulting react app has no real value, but it's deployed to GitLab pages anyways:
<https://glt24-sw-deps.gitlab.io/npm-demo/>

This repo's purpose is to serve as a demonstrator.
Don't use it as a template - use it to figure out the bits and pieces.

Examples of things that are not what we would recommend:

- GitLab CI file could be written cleaner and more robust
- Generation of the SBOM file should not use [@cyclonedx/cyclonedx-npm](https://www.npmjs.com/package/@cyclonedx/cyclonedx-npm) but [@cyclonedx/webpack-plugin](https://www.npmjs.com/package/@cyclonedx/webpack-plugin) adapted for react
